# List of bugs and attachments:

- OC_01_Despite drawing two 6s, no win.
### 'Bugs Report/Video/6twotimesinrowandnoaction.mov'

- OC_02_The "Play again" button / function does not work even though the name of player is entered.
### 'Bugs Report/Screenshots/playagainitsnotworking.png'

- OC_03_When throwing, the cube's condition resets or shows an incorrect value.
### 'Bugs Report/Video/conditionresetsincorrectvalue.mov'

- OC_04_A typo in the winning information.
### 'Bugs Report/Screenshots/typoinprize.png'

- [UX Suggestion] It's shouldn't be possible to roll the dice without giving the name.
### 'Bugs Report/Screenshots/rollwithoutname.png'