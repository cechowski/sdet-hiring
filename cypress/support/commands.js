Cypress.Commands.add('visitPage', () => {
	cy.visit('/');
	cy.location().should((loc) => {
		expect(loc.href).to.eq('http://localhost:3000/');
	});
});

// command added to skip entering the address and verify the address after opening
