export const titleHeader = 'h2';
export const introduceHeader = 'div > p';
export const rollCommunicate = ':nth-child(2) > p';
export const saveName = 'div > button';
export const rollButton = ':nth-child(2) > button';
export const inputName = 'input';
export const namePlaceHolder = 'tr > :nth-child(1)';
export const movingText = 'marquee';
