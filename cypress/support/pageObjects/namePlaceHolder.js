import { inputName, saveName, movingText } from '../pageElements';
import { name } from '../testData';

export class namePlaceHolder {
	emptyInput() {
		cy.get(inputName).then((inputName) => {
			expect(inputName).to.be.visible.and.have.value('');
		});
	}
	fillingInput() {
		cy.get(inputName).type(name);
		cy.get(saveName).click();
	}
	filledInput() {
		cy.get(movingText).then((movingText) => {
			expect(movingText).to.be.visible.and.have.text('Welcome ' + name);
		});
	}
	fillingErr() {
		cy.get(inputName).type(err);
		cy.get(saveName).click();
	}
}
export const onNamePlaceHolder = new namePlaceHolder();
