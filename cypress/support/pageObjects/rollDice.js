import { rollButton, rollCommunicate } from '../pageElements';
export class rollDice {
	buttonRollDice() {
		cy.get(rollButton).then((rollButton) => {
			expect(rollButton).to.be.visible;
			cy.get(rollButton).click();
		});
	}
	//it's depends if we know that starting value is always the same
	firstScoreRollDice() {
		cy.get(rollCommunicate).then((rollCommunicate) => {
			expect(rollCommunicate).to.have.text('Current roll: 2');
		});
	}
	properScoreRollDice() {
		cy.get(rollCommunicate).then((rollCommunicate) => {
			expect(rollCommunicate).to.contain('Current roll:');
		});
	}
	rollrollDice() {
		cy.get(rollButton).click();
	}
}

export const onRollDice = new rollDice();
