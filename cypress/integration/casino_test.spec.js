import '../support/commands';
import { onNamePlaceHolder } from '../support/pageObjects/namePlaceHolder';
import { onRollDice } from '../support/pageObjects/rollDice';
import {
	titleHeader,
	introduceHeader,
	rollCommunicate,
	saveName,
	rollButton,
	inputName
} from '../support/pageElements';

describe('Treeline QA Automation task', () => {
	beforeEach('open aplication', () => {
		cy.visitPage();
	});
	it('01.User is able to successfully open the app and see their elements', () => {
		cy.visitPage(); //visiting mechanism you can find in commands

		//checking visible elements of page without page objects:
		cy.get(titleHeader).then((titleHeader) => {
			expect(titleHeader).to.be.visible.and.have.text('Welcome to online cassino');
		});
		cy.get(introduceHeader).then((introduceHeader) => {
			expect(introduceHeader).to.be.visible.and.have.text('Introduce yourself!');
		});
		cy.get(rollCommunicate).then((rollCommunicate) => {
			expect(rollCommunicate).to.be.visible.and.have.text('You must roll the dice first');
		});
		cy.get(inputName).then((inputName) => {
			expect(inputName).to.be.visible.and.have.value('');
		});
		cy.get(saveName).then((saveName) => {
			expect(saveName).to.be.visible.and.have.text('Save name');
		});
		cy.get(rollButton).then((rollButton) => {
			expect(rollButton).to.be.visible.and.have.text('Roll the dice');
		});
	});
	it('02.User is able to successfully roll the dice without type and save his name.', () => {
		onNamePlaceHolder.emptyInput();
		onRollDice.buttonRollDice();
		onRollDice.firstScoreRollDice();
	});
	it('03.User is able to successfully type and save his name and roll the dice', () => {
		onNamePlaceHolder.emptyInput();
		onNamePlaceHolder.fillingInput();
		onNamePlaceHolder.filledInput();
		onRollDice.buttonRollDice();
		onRollDice.firstScoreRollDice();
	});
	it('04.Attempt to do 30 throws of the dice in a row', () => {
		cy.get(rollButton).click();

		// For loop will run 30 times & after each 0,5 second
		// it will check contents of element and will try to invoke an error

		var i;
		for (i = 0; i < 30; i++) {
			cy.wait(500);
			cy.get(rollButton).click();
			cy.get(rollCommunicate).each(($text) => {
				const lastupdate = $text.text();
				cy.log(lastupdate);

				if (expect($text.text()).to.contains('Current roll:')) {
					cy.log('Ok!');
					return;
				} else;
				{
					return;
				}
				return false;
			});
		}
	});
});
